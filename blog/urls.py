from django.conf.urls import url
from . import views

app_name = 'blog'

urlpatterns = [
    url('^index/', views.index, name='index'),
    url('^hello/', views.hello, name='hello')
]