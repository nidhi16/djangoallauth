from django.shortcuts import render
from django.contrib.auth.decorators import login_required


@login_required(login_url='login')
def index(request):
    return render(request, 'blog/index.html')


@login_required(login_url='login')
def hello(request):
    return render(request, 'blog/hello.html')